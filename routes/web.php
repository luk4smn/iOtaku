<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'Auth\RedirectController@redirect')->middleware('auth');

Route::group(['middleware' => ['auth']], function () {

    //PAGINA PRINCIPAL
    Route::get('/home', 'HomeController@index')
        ->name('home');

    //PERFIL PESSOAL
    Route::get('/my-profile', 'UserController@myProfile')
        ->name('myprofile.index');

    Route::get('/my-profile/edit', 'UserController@myProfileEdit')
        ->name('myprofile.edit');

    //COLEÇÕES AMIGOS E REVISTAS
    Route::get('colecoes/{colecao}/itens', 'ColecaoController@listItens')
        ->name('colecoes.itens.list');

    Route::get('amigos/list', 'AmigoController@list')
        ->name('amigos.list');

    Route::get('revistas/list', 'RevistaController@list')
        ->name('revistas.list');

    Route::get('revistas/tipos', 'RevistaController@tipos')
        ->name('revistas.tipos');

    Route::post('revistas/{revista}/marcar-para-venda', 'RevistaController@marcarParaVenda')
        ->name('revistas.marcar-para-venda');

    Route::post('revistas/{revista}/desmarcar-para-venda', 'RevistaController@desmarcarParaVenda')
        ->name('revistas.desmarcar-para-venda');

    Route::post('emprestimos/{emprestimo}/devolver', 'EmprestimoController@devolver')
        ->name('emprestimos.devolver');

    Route::get('users/list', 'UserController@list')
        ->name('users.list');

    Route::get('search', 'BuscaController@index')
        ->name('search.index');

    //CRUDS
    Route::resource('users', 'UserController');
    Route::resource('titulos', 'TituloController');
    Route::resource('colecoes', 'ColecaoController');
    Route::resource('revistas', 'RevistaController');
    Route::resource('amigos', 'AmigoController');
    Route::resource('emprestimos', 'EmprestimoController');
    Route::resource('mensagens', 'MensagemController');
    Route::resource('comentarios', 'ComentarioController');
    Route::resource('lista', 'ListaDeDesejosController');

});