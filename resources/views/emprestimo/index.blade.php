@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Emprestimos</li>
    </ol>
@endsection

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-book"></i>
            {{ ($pendentes ?? false) ? 'Empréstimos Pendentes' : 'Empréstimos'}}
        </h1>
        <a class="btn btn-success btn-add-new openModal"
           data-toggle="modal"
           data-target="#formModal"
           data-action="{{ route('emprestimos.store') }}"
           data-method="{{ 'POST' }}"
           data-title="{{ " Adicionar Cadastro " }}"
        >
            <i href="#" class="not-active voyager-plus"></i> {{ 'Adicionar novo emprestimo' }}
        </a>
    </div>

    <div id="gradient_bg"></div>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <table  class="table datatable-export" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Data de empréstimo</th>
                                            <th>Data de devolução</th>
                                            <th>Amigo</th>
                                            <th>Itens emprestados</th>
                                            <th>Descrições / Observações</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($emprestimos as $emprestimo)
                                            <tr>
                                                <td>{{ $emprestimo->id }}</td>
                                                <td>{{ date('d/m/Y', strtotime($emprestimo->data_emprestimo)) ?? '' }}</td>
                                                <td>{{ isset($emprestimo->data_devolucao) ? date('d/m/Y', strtotime($emprestimo->data_devolucao)) : '' }}</td>
                                                <td>{{ $emprestimo->amigo->nome }}</td>
                                                <td>
                                                    @foreach($emprestimo->itens as $item )
                                                        <strong>{{ $item->revista->colecao->titulo->nome  .' - '. $item->revista->numero_volume .' - '. $item->revista->sub_titulo .'; ' }}</strong>
                                                    @endforeach

                                                </td>
                                                <td class="col-lg-5" style="text-align: center">{{ $emprestimo->descricao }}</td>

                                                <td id="bread-actions" align="center">
                                                    @if(!$emprestimo->data_devolucao)
                                                    <button title="Marcar Devolução" class="btn btn-sm btn-success pull-center devolver"
                                                            href="#"
                                                            data-action="{{ route('emprestimos.devolver', $emprestimo->id) }}"
                                                            data-redirect="{{ route('emprestimos.index') }}"
                                                    >
                                                        <i class="not-active voyager-check"></i>
                                                    </button>
                                                    @endif

                                                    <button title="Editar" class="btn btn-sm btn-primary pull-center edit openModal"
                                                            data-toggle="modal"
                                                            data-target="#formModal"
                                                            data-action="{{route('emprestimos.update', $emprestimo->id)}}"
                                                            data-method="{{'PUT'}}"
                                                            data-title="{{ " Alterar empréstimo " }}"
                                                            data-id="{{ $emprestimo->id }}"
                                                    >
                                                        <i class="not-active voyager-edit" disabled="true"></i>
                                                    </button>

                                                    <button title="Delete" class="btn btn-sm btn-danger pull-center delete"
                                                            href="#"
                                                            data-action="{{ route('emprestimos.destroy', $emprestimo->id) }}"
                                                            data-redirect="{{ route('emprestimos.index') }}"
                                                    >
                                                        <i class="not-active voyager-trash"></i>
                                                    </button>

                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7" align="center">Nenhum dado cadastrado até o momento</td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <emprestimo></emprestimo>


@endsection