@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Perfil</li>
        <li>Editar</li>
    </ol>
@endsection

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-person"></i>{{'Editar Perfil'}}
    </h1>
@endsection

@section('content')

    <div class="page-content container-fluid">
        {{ Form::model(auth()->user(), ['route' => ['users.update', auth()->user()->id],'class'=>'form-horizontal', 'enctype'=>'multipart/form-data', 'method' => 'PUT']) }}

        {{ csrf_field() }}
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif


            <div class="col-md-8">
                <div class="panel panel-bordered">
                    <div class="panel-heading new-setting">
                        <h3 class="panel-title"><i class="voyager-plus"></i> Dados do usuário</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-lg-6">
                                <label for="name">Nome</label>
                                {!! Form::text('name', old('name'), ['class'=>'form-control','placeholder'=>'Nome']) !!}
                            </div>
                            <div class="col-lg-6">
                                <label for="email">Email</label>
                                {!! Form::text('email', old('email'), ['class'=>'form-control','placeholder'=>'Email']) !!}
                            </div>
                        </div>

                        <div class="form-group form-group-default" id="passwordGroup">
                            <div class="col-md-12">
                                <label for="password">Senha</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" placeholder="Só preencha se pretende alterar sua senha atual">
                            </div>
                        </div>

                        <div class="form-group form-group-default" id="confirmPasswordGroup">
                            <div class="col-md-12">
                                <label for="password-confirm">Confirmar a senha</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel panel-bordered">
                    <div class="panel-heading new-setting">
                        <h3 class="panel-title"><i class="voyager-images"></i> Imagem de perfil</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group" align="center">
                            <img src="{{ auth()->user()->avatar ?? asset('vendor/images/icon-no-image.png') }}"
                                 style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                            <input type="file" data-name="avatar" name="avatar">
                        </div>
                    </div>
                    {{--<input id="avatar-2" name="avatar-2" type="file" required>--}}
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{'Salvar'}}
            </button>
        </div>

    </div>
@endsection
