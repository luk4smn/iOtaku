@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Perfil</li>
    </ol>
@endsection

@section('content')
    <div style="background-size:cover; background: url(/vendor/images/bg2.jpg) center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:160px; display:block; width:100%"></div>
    <div style="position:relative; z-index:9; text-align:center;">
        <img src="{{ auth()->user()->avatar ?? asset('/vendor/images/user_default.png') }}"
             class="avatar"
             style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;"
             alt="{{ Auth::user()->name }} avatar">
        <h4>{{ ucwords(Auth::user()->name) }}</h4>
        <div class="user-email text-muted">{{ ucwords(Auth::user()->email) }}</div>
        <p>Membro desde: {{ Auth::user()->created_at->format('d/m/Y') }}</p>
        <a href="{{route('myprofile.edit')}}" class="btn btn-primary">Editar Perfil</a>
    </div>
@endsection