@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Perfil</li>
    </ol>
@endsection

@section('content')
    <div style="background-size:cover; background: url(/vendor/images/bg2.jpg) center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:160px; display:block; width:100%"></div>
    <div style="position:relative; z-index:9; text-align:center;">
        <img src="{{ $user->avatar ?? asset('/vendor/images/user_default.png') }}"
             class="avatar"
             style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;"
             alt="{{ $user->name }} avatar">
        <h4>{{ ucwords($user->name) }}</h4>
        <div class="user-email text-muted">{{ ucwords($user->email) }}</div>
        <p>Membro desde: {{ $user->created_at->format('d/m/Y') }}</p>
        <a href="{{route('colecoes.index', ['user_id' => $user->id])}}" class="btn btn-primary">Ver Coleções</a>
    </div>

    <div class="page-content">
        <div class="alerts">
        </div>
        <div class="analytics-container">
            {{ Form::open(['route' => ['mensagens.store'],'class'=>'form-horizontal', 'method' => 'POST']) }}

            {{ csrf_field() }}
            <div class="form-group">
                <div class="col-lg-12">
                    <label for="descricao"><h5>Envie uma mensagem:</h5></label>
                    {!! Form::hidden('user_id', $user->id ?? null) !!}
                    {!! Form::textarea('mensagem', '',['class'=>'form-control', 'rows' => 3,'placeholder'=>'Mensagem' , 'required']) !!}
                    <br>
                    <button type="submit" class="btn btn-primary pull-right">
                        <i class="voyager-paper-plane"></i>
                        <span> Enviar</span>
                    </button>
                </div>
            </div>

        </div>
    </div>


            <div class="panel-body">


            </div>

@endsection