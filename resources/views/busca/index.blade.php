@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Busca</li>
    </ol>
@endsection

@section('page_header')
    <h3 class="page-title">
        <i class="voyager-compass"></i>
        <p>{{ 'Buscar' }}</p>
    </h3>

    <div id="gradient_bg"></div>
@endsection


@section('content')
    <div class="page-content conteudo compass container-fluid">
        <div class="row">

        @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissible fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ HTML::ul($errors->all()) }}
            </div>
        @endif

        </div>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#revistas" aria-expanded="true"><i class="voyager-book"></i> Revistas</a></li>
            <li class=""><a data-toggle="tab" href="#commands" aria-expanded="false"><i class="voyager-people"></i> Usuários</a></li>
        </ul>

        <div class="tab-content">
            <div id="revistas" class="tab-pane fade active in">
                <h3>
                    <i class="voyager-book"></i><span>{{ ' Revistas' }}</span>
                </h3>
                <h3>
                    <small>Busque por titulos de mangás e manhwhas.</small>
                </h3>

                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">

                                <search-revista></search-revista>

                        </div>
                    </div>
                </div>

                <div class="collapsible">
                    <div class="collapse-head" data-toggle="collapse" data-target="#links" aria-expanded="true" aria-controls="links">
                        <small>...</small>
                        <i class="voyager-angle-down"></i>
                        <i class="voyager-angle-up"></i>
                    </div>
                    <div class="collapse-content collapse in" id="links">
                        <div class="row">
                            <div class="col-md-4">
                                <a target="_blank" class="voyager-link" style="background-image:url( {{ asset('vendor/images/compass/documentation.jpg') }})">
                                    <span class="resource_label"><i class="voyager-documentation"></i> <span class="copy">Busque</span></span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a target="_blank" class="voyager-link" style="background-image:url( {{ asset('vendor/images/compass/voyager-home.jpg') }} )">
                                    <span class="resource_label"><i class="voyager-bubble"></i> <span class="copy">Comente</span></span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a target="_blank" class="voyager-link" style="background-image:url( {{ asset('vendor/images/compass/hooks.jpg') }} )">
                                    <span class="resource_label"><i class="voyager-gift"></i> <span class="copy">Marque como desejado</span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="commands" class="tab-pane fade">
                <h3>
                    <i class="voyager-person"></i> Usuários
                    <small>Busque por usuários.</small>
                </h3>

                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">

                            <search-user></search-user>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection