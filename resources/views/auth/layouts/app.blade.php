<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="admin login">
    <title>{{env('APP_NAME')}}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('vendor/css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    {!! HTML::script('vendor/js/app.js')!!}

    <style>
        body {
            background-image:url('{{ URL::asset('vendor/images/bg.jpg') }}');
            background-color: #FFFFFF;
        }
        .login-sidebar{
            border-top:5px solid #22A7F0;
        }
        @media (max-width: 767px) {
            .login-sidebar {
                border-top:0px !important;
                border-left:5px solid #22A7F0;
            }
        }
        body.login .form-group-default.focused{
            border-color:#22A7F0;
        }
        .login-button, .bar:before, .bar:after{
            background:#22A7F0;
        }
    </style>
</head>

<body class="login">
<div class="container-fluid">
    <div class="row">
        <div class="faded-bg animated"></div>
        <div class="hidden-xs col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2">
                    <div class="logo-title-container">
                        <img class="img-responsive pull-left logo hidden-xs animated fadeIn" src="{{ URL::asset('vendor/images/logo-icon-light.png') }}" alt="Logo Icon">
                        <div class="copy animated fadeIn">
                            <h1>{{ env('APP_NAME') }}</h1>
                            <p>Sistema de controle de mangás e manhwas</p>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>
        </div>
        @yield('content')
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<script>
    let btn = document.querySelector('button[type="submit"]');
    let form = document.forms[0];
    let email = document.querySelector('[name="email"]');
    let password = document.querySelector('[name="password"]');

    btn.addEventListener('click', function(ev){
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });

    email.focus();
    document.getElementById('emailGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function(e){
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
        document.getElementById('emailGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
        document.getElementById('passwordGroup').classList.remove("focused");
    });

</script>
</body>

</html>
