@extends('auth.layouts.app')

@section('content')


    <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

        <div class="login-container">

            <p>Preencha os dados para realizar o seu cadastro:</p>

            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group form-group-default" id="nameGroup">
                    <label>Nome</label>

                    <div class="col-md-12">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback text-danger">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-default" id="emailGroup">
                    <label>E-mail</label>

                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-danger">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-default" id="passwordGroup">
                    <label>Senha</label>

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback text-danger">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-default" id="confirmPasswordGroup">
                    <label>Confirmar a senha</label>

                    <div class="col-md-12">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>

                <button type="submit" class="btn btn-block login-button">
                    <span class="signingin hidden"><span class="voyager-refresh"></span> Enviando Registro...</span>
                    <span class="signin">Registrar</span>
                </button>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-0">
                        <a class="btn btn-link" style="font-size: smaller" href="{{ route('login') }}">
                            {{ __('Voltar à tela de Login') }}
                        </a>
                    </div>
                </div>

            </form>

            <div style="clear:both"></div>


        </div> <!-- .register-container -->

    </div> <!-- .register-sidebar -->

@endsection
