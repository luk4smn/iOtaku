@extends('auth.layouts.app')

@section('content')

    <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

        <div class="login-container">

            <p>Resetar Senha</p>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-group form-group-default" id="emailGroup">
                    <label>E-mail</label>
                    <div class="controls">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <button type="submit" class="btn btn-block login-button">
                    <span class="signingin hidden"><span class="voyager-refresh"></span> Enviando...</span>
                    <span class="signin">Enviar Link</span>
                </button>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-0">
                        <a class="btn btn-link" style="font-size: smaller" href="{{ route('login') }}">
                            {{ __('Voltar à tela de Login') }}
                        </a>
                    </div>
                </div>

            </form>

            <div style="clear:both"></div>

        </div> <!-- .reset-container -->

    </div> <!-- .reset-sidebar -->
@endsection
