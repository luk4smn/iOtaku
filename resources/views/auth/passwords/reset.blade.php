@extends('auth.layouts.app')

@section('content')
    <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

        <div class="login-container">

            <p>Resetar Senha</p>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.request') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group form-group-default" id="emailGroup">
                    <label>E-mail</label>

                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-default" id="passwordGroup">
                    <label>Nova Senha</label>

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-default" id="passwordGroup">
                    <label>Confirmar Nova Senha</label>

                    <div class="col-md-12">
                        <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <button type="submit" class="btn btn-block login-button">
                    <span class="signingin hidden"><span class="voyager-refresh"></span> Enviando...</span>
                    <span class="signin">Resetar Senha</span>
                </button>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-0">
                        <a class="btn btn-link" style="font-size: smaller" href="{{ route('login') }}">
                            {{ __('Voltar à tela de Login') }}
                        </a>
                    </div>
                </div>

            </form>

            <div style="clear:both"></div>

        </div> <!-- .reset-container -->

    </div> <!-- .reset-sidebar -->

@endsection
