@extends('auth.layouts.app')

@section('content')

    <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">

        <div class="login-container">

            <p>Preencha os dados de login abaixo:</p>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group form-group-default" id="emailGroup">
                    <label>E-mail</label>
                    <div class="controls">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-default" id="passwordGroup">
                    <label>Senha</label>
                    <div class="controls">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div>
                    <button type="submit" class="btn btn-block login-button">
                        <span class="signingin hidden"><span class="voyager-refresh"></span> Efetuando Login...</span>
                        <span class="signin">Login</span>
                    </button>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-0">
                        <a class="btn btn-link" style="font-size: smaller" href="{{ route('password.request') }}">
                            {{ __('Esqueci a senha') }}
                        </a>
                        <a class="btn btn-link" style="font-size: smaller" href="{{ route('register') }}">
                            {{ __('Registrar') }}
                        </a>
                    </div>
                </div>
            </form>

            <div style="clear:both"></div>
        </div> <!-- .login-container -->

    </div> <!-- .login-sidebar -->
@endsection
