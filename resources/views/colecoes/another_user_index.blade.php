@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Coleções</li>
    </ol>
@endsection

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-archive"></i>
            <span> {{ 'Coleções do usuario: ' .$nome }}</span>
        </h1>
    </div>

    <div id="gradient_bg"></div>
@endsection


@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <table  class="table datatable-export" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Autor</th>
                                            <th>Resumo</th>
                                            <th>Nº de volumes lançados</th>
                                            <th>Nº de itens da coleção</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($colecoes as $colecao)
                                            <tr>
                                                <td>{{ $colecao->titulo->nome }}</td>
                                                <td>{{ $colecao->titulo->autor }}</td>
                                                <td class="col-lg-5" style="text-align: left">{{ $colecao->titulo->resumo }}</td>
                                                <td>{{ $colecao->total_volumes }}</td>
                                                <td>{{ $colecao->itens->count() }}</td>
                                                <td id="bread-actions" align="center">

                                                    <a class="btn btn-sm btn-success pull-center view"
                                                       href="{{ route('colecoes.itens.list', $colecao->id) }}"
                                                       title="Vizualizar"
                                                    >
                                                        <i class="not-active voyager-list"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6" align="center">Nenhum dado cadastrado até o momento</td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection