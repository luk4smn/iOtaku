@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
           <i class="voyager-home"></i> Principal</a>
        </li>
    </ol>
@endsection

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-anchor"></i>{{ 'Bem vindo: '.auth()->user()->name }}
        </h1>

    </div>

    <div id="gradient_bg"></div>
@endsection

@section('content')
    <div class="alerts">
    </div>

    <div class="clearfix container-fluid row">
        <div class='col-xs-12 col-sm-6 col-md-4'><div class="panel widget center bgimage"
            style="margin-bottom:0;overflow:hidden;background-image:url({{ url('vendor/images/widget-backgrounds/01.jpg')}});">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class='voyager-search'></i>
                    <h4>Buscar</h4>
                    <p>Busque por usuários ou obras do seu interesse.</p>
                    <a href="{{ route('search.index') }}" class="btn btn-primary">Visualizar</a>
                </div>
            </div>
        </div>

        <div class='col-xs-12 col-sm-6 col-md-4'><div class="panel widget center bgimage"
            style="margin-bottom:0;overflow:hidden;background-image:url({{ url('vendor/images/widget-backgrounds/02.jpg')}});">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class='voyager-mail'></i>
                    <h4>Mensagens</h4>
                    <p>{{ $mensagens }} Mensagens não lidas.</p>
                    <a href="{{ route('mensagens.index') }}" class="btn btn-primary">Visualizar</a>
                </div>
            </div>
        </div>

        <div class='col-xs-12 col-sm-6 col-md-4'><div class="panel widget center bgimage"
            style="margin-bottom:0;overflow:hidden;background-image:url({{ url('vendor/images/widget-backgrounds/03.jpg')}});">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <i class='voyager-book'></i>
                    <h4>Empréstimos Pendentes</h4>
                    <p>{{ $pendentes }} Emprestimos com entregas pendendes.</p>
                    <a href="{{ route('emprestimos.index', ['pendentes' => true]) }}" class="btn btn-primary">Visualizar</a>
                </div>
            </div>
        </div>

    </div>
@endsection