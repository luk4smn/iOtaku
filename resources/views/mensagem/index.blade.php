@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Mensagens</li>
    </ol>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="container">
                        <h3 class=" text-center">Messenger</h3>
                        <div class="messaging">
                            <div class="inbox_msg">
                                <div class="inbox_people">
                                    <div class="headind_srch">
                                        <div class="recent_heading">
                                            <h4>Recentes</h4>
                                        </div>
                                    </div>
                                    <div class="inbox_chat">
                                        @foreach($conversas as $conversa)
                                        <div class="chat_list">
                                            <div class="chat_people">
                                                <div class="chat_img"> <img src="{{ end($conversa)->remetente->avatar ?? asset('vendor/images/user_default.png') }}" alt="sunil"> </div>
                                                <div class="chat_ib">
                                                    <h5>{{ end($conversa)->remetente->name }} <span class="chat_date">{{ end($conversa)->created_at->format('d-m-Y H:m:i') }}</span></h5>
                                                    <p>{{ end($conversa)->mensagem }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="mesgs">
                                    <div class="msg_history">
                                        <div class="incoming_msg">

                                        </div>

                                        {{--<div class="incoming_msg">--}}
                                            {{--<div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>--}}
                                            {{--<div class="received_msg">--}}
                                                {{--<div class="received_withd_msg">--}}
                                                    {{--<p>Test, which is a new approach to have</p>--}}
                                                    {{--<span class="time_date"> 11:01 AM    |    Yesterday</span></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="outgoing_msg">--}}
                                            {{--<div class="sent_msg">--}}
                                                {{--<p>Apollo University, Delhi, India Test</p>--}}
                                                {{--<span class="time_date"> 11:01 AM    |    Today</span> </div>--}}
                                        {{--</div>--}}

                                    </div>
                                    <div class="type_msg">
                                        <div class="input_msg_write">
                                            <input type="text" class="write_msg" placeholder="Escreva a mensagem" />
                                            <button class="msg_send_btn" type="button"><i class="voyager-paper-plane" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection