@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Amigos</li>
    </ol>
@endsection

@section('page_header')

    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-people"></i>
            <p>{{'Amigos'}}</p>
        </h1>
        <a class="btn btn-success btn-add-new openModal"
           data-toggle="modal"
           data-target="#formModal"
           data-action="{{ route('amigos.store') }}"
           data-method="{{ 'POST' }}"
           data-title="{{ " Adicionar Cadastro " }}"
        >
            <i class="not-active voyager-plus"></i> {{ 'Adicionar novo' }}
        </a>
    </div>

    <div id="gradient_bg"></div>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <table  class="table datatable-export" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Endereço</th>
                                            <th>Telefone</th>
                                            <th>E-mail</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($amigos as $amigo)
                                            <tr>
                                                <td>{{ $amigo->nome }}</td>
                                                <td class="col-lg-5" style="text-align: left">{{ $amigo->endereco }}</td>
                                                <td>{{ $amigo->telefone }}</td>
                                                <td>{{ $amigo->email }}</td>
                                                <td id="bread-actions" align="center">

                                                    <button title="Editar" class="btn btn-sm btn-primary pull-center edit openModal"
                                                            data-toggle="modal"
                                                            data-target="#formModal"
                                                            data-action="{{route('amigos.update', $amigo->id)}}"
                                                            data-method="{{'PUT'}}"
                                                            data-title="{{ " Alterar Cadastro " }}"
                                                            data-id="{{ $amigo->id }}"
                                                    >
                                                        <i class="not-active voyager-edit" disabled="true"></i>
                                                    </button>

                                                    <button title="Delete" class="btn btn-sm btn-danger pull-center delete"
                                                            href="#"
                                                            data-action="{{ route('amigos.destroy', $amigo->id) }}"
                                                            data-redirect="{{ route('amigos.index') }}"
                                                    >
                                                        <i class="not-active voyager-trash"></i>
                                                    </button>

                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6" align="center">Nenhum dado cadastrado até o momento</td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <amigo></amigo>


@endsection