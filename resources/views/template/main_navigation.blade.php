
<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <div class="logo-icon-container">
                        <img src="{{ URL::asset('vendor/images/logo-icon-light.png') }}" alt="Logo Icon">
                    </div>
                    <div class="title">{{env('APP_NAME') ?? 'iOtaku'}}</div>
                </a>
            </div><!-- .navbar-header -->

            <div class="panel widget center bgimage"
                 style="background-image:url(/vendor/images/bg.jpg); background-size: cover;
                 background-position: 0px;">

                <div class="dimmer"></div>
                <div class="panel-content">
                    <img src="{{ auth()->user()->avatar ?? URL::asset('vendor/images/user_default.png') }}"
                         class="avatar" alt="Avatar"
                         style="width: 40px; height: 40px;background-position: 50% 50%; background-size: auto 100%; border:2px solid #14ff26;"
                    >
                    <h4>&emsp; {{ auth()->user()->name }}</h4>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>

        <ul class="nav navbar-nav">
            <li class="active">
                <a href="{{url('/')}}" target="_self">
                    <span class="icon voyager-home"></span>
                    <span class="title">Principal</span>
                </a>
            </li>

            <li class="">
                <a href="{{ route('colecoes.index') }}" target="_self">
                    <span class="icon voyager-archive"></span>
                    <span class="title">Coleções</span>
                </a>
            </li>

            <li class="">
                <a href="{{ route('amigos.index') }}" target="_self">
                    <span class="icon voyager-people"></span>
                    <span class="title">Amigos</span>
                </a>
            </li>

            <li class="">
                <a href="{{ route('emprestimos.index') }}" target="_self">
                    <span class="icon voyager-book"></span>
                    <span class="title">Empréstimos</span>
                </a>
            </li>

            <li class="">
                <a href="{{ route('lista.index') }}" target="_self">
                    <span class="icon voyager-gift"></span>
                    <span class="title">Minha Lista</span>
                </a>
            </li>

        </ul>

    </nav>
</div>