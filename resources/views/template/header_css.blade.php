<!-- App CSS -->
<link rel="stylesheet" href="{{ URL::asset('vendor/css/app.css') }}">

{{-- meta for vue js forms--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Few Dynamic Styles -->
<style type="text/css">
    .voyager .side-menu .navbar-header {
        background:#22A7F0;
        border-color:#22A7F0;
    }
    .widget .btn-primary{
        border-color:#22A7F0;
    }
    .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
        background:#22A7F0;
    }
    .voyager .breadcrumb a{
        color:#22A7F0;
    }
    thead th, thead td {
        text-align: center;
    }
    tbody th, tbody td {
        text-align: center;
    }
    tfoot th, tfoot td {
        text-align: center;
    }
    .app-footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
    }
    .not-active {
        pointer-events: none;
        cursor: not-allowed;
        text-decoration: none;
    }
    .container{margin:auto;}
    img{ max-width:100%;}
    .inbox_people {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 40%; border-right:1px solid #c4c4c4;
    }
    .inbox_msg {
        border: 1px solid #c4c4c4;
        clear: both;
        overflow: hidden;
    }
    .top_spac{ margin: 20px 0 0;}


    .recent_heading {float: left; width:40%;}
    .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%; padding:
    }
    .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
    }
    .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
    .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
    }
    .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

    .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
    .chat_ib h5 span{ font-size:13px; float:right;}
    .chat_ib p{ font-size:14px; color:#989898; margin:auto}
    .chat_img {
        float: left;
        width: 11%;
    }
    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people{ overflow:hidden; clear:both;}
    .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
    }
    .inbox_chat { height: 550px; overflow-y: scroll;}

    .active_chat{ background:#ebebeb;}

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }
    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }
    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }
    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }
    .received_withd_msg { width: 57%;}
    .mesgs {
        float: left;
        padding: 30px 15px 0 25px;
        width: 60%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0; color:#fff;
        padding: 5px 10px 5px 12px;
        width:100%;
    }
    .outgoing_msg {
        overflow:hidden; margin:26px 0 26px;
    }
    .sent_msg {
        float: right;
        width: 46%;
    }
    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
    }

    .type_msg {
        border-top: 1px solid #c4c4c4;position: relative;
    }
    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 11px;
        width: 33px;
    }
    .messaging {
        padding: 0 0 50px 0;
    }
    .msg_history {
        height: 516px;
        overflow-y: auto;
    }

</style>

<style type="text/css">
    .voyager .compass .nav-tabs{
        background:none;
        border-bottom:0px;
    }

    .voyager .compass .nav-tabs > li{
        margin-bottom:-1px !important;
    }

    .voyager .compass .nav-tabs a{
        text-align: center;
        font-size: 10px;
        font-weight: normal;
        background: #f8f8f8;
        border: 1px solid #f1f1f1;
        position: relative;
        top: -1px;
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .voyager .compass .nav-tabs a i{
        display: block;
        font-size: 22px;
    }

    .tab-content{
        background:#ffffff;
        border: 1px solid transparent;
        border-radius: 4px;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.05);
    }

    .page-title{
        z-index:2;
        position:relative;
    }

    .conteudo{
        z-index:2;
        position:relative;
    }


    .btn{
        z-index:2;
        position:relative;
    }


    #gradient_bg{
        position: fixed;
        top: 61px;
        left: 0px;
        background-image: url(/vendor/images/bg2.jpg);
        background-size: cover;
        background-position: 0px;
        width: 100%;
        height: 220px;
        z-index: 0;
    }

    #gradient_bg::after{
        content:'';
        position:absolute;
        left:0px;
        top:0px;
        width:100%;
        height:100%;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f8f8f8+0,f8f8f8+100&0.95+0,1+80 */
        background: -moz-linear-gradient(top, rgba(248,248,248,0.93) 0%, rgba(248,248,248,1) 80%, rgba(248,248,248,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, rgba(248,248,248,0.93) 0%,rgba(248,248,248,1) 80%,rgba(248,248,248,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, rgba(248,248,248,0.93) 0%,rgba(248,248,248,1) 80%,rgba(248,248,248,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f8f8f8', endColorstr='#f8f8f8',GradientType=0 ); /* IE6-9 */
        z-index:1;
    }

    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
        background:#fff !important;
        color:#62a8ea !important;
        border-bottom:1px solid #fff !important;
        top:-1px !important;
    }

    .nav-tabs > li a{
        transition:all 0.3s ease;
    }


    .nav-tabs > li.active > a:focus{
        top:0px !important;
    }

    .voyager .compass .nav-tabs > li > a:hover{
        background-color:#fff !important;
    }

    .voyager-link{
        width: 100%;
        min-height: 220px;
        display: block;
        border-radius: 3px;
        background-position: center center;
        background-size: cover;
        position:relative;
    }

    .voyager-link span.resource_label{
        text-align: center;
        color: #fff;
        display: block;
        position: absolute;
        z-index: 9;
        top: 0px;
        left: 0px;
        width: 100%;
        padding: 0px;
        opacity:0.8;
        transition:all 0.3s ease;
        line-height: 220px;
        height: 100%;
    }

    .voyager-link span.resource_label:hover{
        opacity:1;
    }

    .voyager-link i{
        font-size: 48px;
        margin-right: 0px;
        position: absolute;
        width: 70px;
        height: 70px;
        padding: 10px;
        border-radius: 5px;
        line-height: 55px;
        display: inline-block;
        left: 50%;
        margin-top: -50px;
        margin-left: -35px;
        top: 50%;
        line-height:55px;
        padding:10px;
    }

    .voyager-link span.resource_label:hover i{

        opacity:1;
        transition:all 0.3s linear;
    }

    .voyager-link span.copy{
        position: absolute;
        font-size: 16px;
        left: 0px;
        bottom: 70px;
        line-height: 12px;
        text-transform: uppercase;
        text-align: center;
        width: 100%;
    }

    h3 small{
        font-size: 11px;
        position: relative;
        top: -3px;
        left: 10px;
        color: #999;
    }

    .collapsible{
        margin-top:20px;
        border:1px solid #f7f7f7;
        border-radius:3px;
        display:block;
    }

    .collapse-head{
        border-bottom: 1px solid #f7f7f7;
        border-top-right-radius: 3px;
        padding:5px 15px;
        display:block;
        cursor:pointer;
        background:#ffffff;
        position:relative;
    }

    .collapse-head:after{
        content:'';
        clear:both;
        display:block;
    }

    .collapse-head h4{
        float:left;
    }

    .collapse-head i{
        font-size: 16px;
        position: absolute;
        top: 13px;
        right:15px;
        float:right;
    }

    .collapse-content{
        padding:15px;
        background:#fcfcfc;
    }

    .collapse-content .row{
        padding-bottom:0px;
        margin-bottom:0px;
    }

    .collapse-content .col-md-4{
        padding-right:0px;
        margin-bottom:0px;
    }

    .collapse-content .col-md-4:nth-child(3){
        padding-right:15px;
    }

    .voyager-link span.desc{
        font-size:11px;
        color:rgba(255, 255, 255, 0.8);
        width:100%;
        height:100%;
        position:absolute;
        text-align:center;
    }

    .voyager-angle-down{
        display:none;
    }

    .voyager-link::after{
        content:'';
        position:absolute;
        width:100%;
        height:100%;
        background: -moz-linear-gradient(-65deg, rgba(17,17,17,0.7) 0%, rgba(35,35,47,0.7) 50%, rgba(17,17,23,0.7) 51%, rgba(17,17,23,0.7) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(-65deg, rgba(17,17,17,0.7) 0%,rgba(35,35,47,0.7) 50%,rgba(17,17,23,0.7) 51%,rgba(17,17,23,0.7) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(155deg, rgba(17,17,17,0.7) 0%,rgba(35,35,47,0.7) 50%,rgba(17,17,23,0.7) 51%,rgba(17,17,23,0.7) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3111111', endColorstr='#b3111117',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
        left:0px;
        top:0px;
        border-radius:3px;
    }

    #fonts ul{
        list-style:none;
        display:flex;
        padding-left:10px;
        flex-wrap:wrap;
        justify-content:flex-start;
    }

    #fonts .icon{
        float: left;
        padding: 2px;
        font-size: 20px;
        padding-right: 10px;
        position: absolute;
        left: 0px;
    }

    #fonts li{
        flex: 1;
        max-width:212px;
        padding: 10px;
        padding-left: 30px;
        position: relative;
    }

    #fonts .voyager-angle-down{
        display:block;
    }

    #fonts h2{
        font-size: 12px;
        padding: 20px;
        padding-top: 0px;
        font-weight: bold;
        padding-left:5px;
    }

    #fonts h2:nth-child(2){
        padding-top:20px;
    }

    #fonts input{
        border-radius: 3px;
        border: 1px solid #f1f1f1;
        padding: 3px 7px;
    }

    #command_lists{
        display:flex;
        flex-wrap:wrap;
    }

    #commands h3{
        width: 100%;
        clear: both;
        margin-bottom: 20px;
    }

    #commands h3 i{
        position: relative;
        top: 3px;
    }

    #commands .command{
        padding: 10px;
        border: 1px solid #f1f1f1;
        border-radius: 4px;
        border-bottom: 2px solid #f5f5f5;
        cursor:pointer;
        transition:all 0.3s ease;
        position:relative;
        padding-top:30px;
        padding-right: 52px;
        flex:1;
        min-width:275px;
        margin:10px;
        margin-left:0px;
    }

    #commands .command.more_args{
        padding-bottom:40px;
    }

    #commands .command i{
        position: absolute;
        right: 4px;
        top: -6px;
        font-size: 45px;
    }

    #commands code{
        color: #549DEA;
        padding: 4px 7px;
        font-weight: normal;
        font-size: 12px;
        background: #f3f7ff;
        border: 0px;
        position: absolute;
        top: 0px;
        left: 0px;
        border-bottom-left-radius: 0px;
        border-top-right-radius: 0px;
    }

    #commands .command:hover{
        border-color:#eaeaea;
        border-bottom-width:2px;
    }



    .cmd_form{
        display:none;
        position:absolute;
        bottom: 0px;
        left: 0px;
        width: 100%;
    }

    .cmd_form input[type="text"], .cmd_form input[type="submit"]{
        width:30%;
        float:left;
        margin: 0px;
        font-size: 12px;
    }

    .cmd_form input[type="text"]{
        line-height: 30px;
        padding-top: 0px;
        padding-bottom: 0px;
        height: 30px;
        border-top-right-radius: 0px;
        border-bottom-right-radius: 0px;
        border-top-left-radius:0px;
        padding-left:5px;
        font-size:12px;
        width:70%;
    }

    .cmd_form .form-control.focus, .cmd_form .form-control:focus{
        border-color:#eee;
    }

    .cmd_form input[type="submit"]{
        border-top-right-radius: 0px;
        border-bottom-left-radius: 0px;
        border-top-left-radius:0px;
        font-size: 10px;
        padding-left: 7px;
        padding-right: 7px;
        height: 30px;
    }


    #commands pre{
        background:#323A42;
        color:#fff;
        width:100%;
        margin:10px;
        margin-left:0px;
        padding: 15px;
        padding-top: 0px;
        padding-bottom: 0px;
        position:relative;
    }

    #commands .close-output{
        position:absolute;
        right:15px;
        top:15px;
        color:#ccc;
        cursor:pointer;
        padding: 5px 14px;
        background: rgba(0, 0, 0, 0.2);
        border-radius: 25px;
        transition:all 0.3s ease;
    }

    #commands .close-output:hover{
        color:#fff;
        background: rgba(0, 0, 0, 0.3);
    }

    #commands pre i:before{
        position:relative;
        top:3px;
        right:5px;
    }

    #commands pre .art_out{
        width: 100%;
        display: block;
        color: #98cb00;
        margin-bottom:10px;
    }

    .box {
        border-bottom:1px solid;
        border-bottom-color: #1e97da;
        width:100%;
    }

    .box input {
        display:inline-block;
        outline:0;
        border:0;
        padding: 5px;
    }

    .box .button {
        background: #aaa;
        color: #fff;
        font-weight: bold;
        border:none;
        float:right;
        padding: 5px 10px;
    }
</style>