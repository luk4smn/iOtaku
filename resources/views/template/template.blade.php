<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{env('APP_NAME')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('vendor/images/logo-icon.png') }}" type="image/x-icon">

    @include('template.header_css')
    @yield('css')

    {{--ziggy library--}}
    @routes
</head>

<body class="voyager">
    <div id="app">
        <div id="voyager-loader">
            <img src="{{ URL::asset('vendor/images/loading.png') }}" alt="Voyager Loader">
        </div>

        <div class="app-container">
            <div class="fadetoblack visible-xs"></div>
            <div class="row content-container">
            @include('template.navbar')
            @include('template.main_navigation')

            <!-- Main Content -->
                <div class="container-fluid">
                    <div class="side-body padding-top">
                        @yield('page_header')
                        <div id="voyager-notifications"></div>
                        <div class="page-content">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->

    @include('template.footer_scripts')
    @include('sweetalert::alert')
    @include('shared.messages')
    @stack('scripts')

    <footer class="app-footer">
        <div class="site-footer-right" style="font-size: smaller">
            <a>Desenvolvido por Lucas Nunes</a> - v1.0
        </div>
    </footer>

</body>
</html>