<nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="hamburger btn-link">
                <span class="hamburger-inner"></span>
            </button>
            @yield('breacumbs')
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown profile">
                <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
                   aria-expanded="false">
                    <img src="{{ URL::asset('vendor/images/helm.png') }}" class="profile-img">
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-animated">
                    <li class="profile-img">
                        <img src="{{ auth()->user()->avatar ?? URL::asset('vendor/images/user_default.png') }}" class="profile-img">
                        <div class="profile-body">
                            <h4 style="font-size: medium">{{auth()->user()->name}}</h4>
                            <a href="{{ route('myprofile.index') }}" >
                                <i class="voyager-person"></i>
                                Perfil do usuário
                            </a>
                        </div>

                    </li>
                    <li class="divider"></li>
                    <li>
                        <form action="{{ route('logout') }}" method="POST" id="logout-form">
                            {{ csrf_field() }}
                            <button class="logout btn btn-danger btn-block">
                                <i class="voyager-power"></i>
                                Logout
                            </button>
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
