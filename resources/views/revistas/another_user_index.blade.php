@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>
            <a href="{{ route('colecoes.index') }}">
                Coleções
            </a>
        </li>
        <li>Itens</li>
    </ol>
@endsection

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-archive"></i>{{ 'Itens da coleção: '.$colecao->titulo->nome .' - Proprietário: '. $colecao->usuario->name }}
        </h1>
    </div>

    <div id="gradient_bg"></div>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <table  class="table datatable-export" style="width:100%" align="center">
                                        <thead>
                                        <tr>
                                            <th>Volume Nº</th>
                                            <th>Miniatura</th>
                                            <th>Subtítulo</th>
                                            <th>Data de lançamento</th>
                                            <th>Tipo</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($itens as $revista)
                                            <tr>
                                                <td>{{ $revista->numero_volume  }}</td>
                                                <td>
                                                    <img src="{{ $revista->imagem  }}" alt="Miniatura" class="capa" style="width: 36px; height: 36px; border: 1px solid rgb(34,166,239);">
                                                </td>
                                                <td>{{ $revista->sub_titulo }}</td>
                                                <td>{{ date('d/m/Y', strtotime($revista->lancamento)) }}</td>
                                                <td>{{ $revista->getTipo() }}</td>

                                                <td id="bread-actions" align="center">
                                                    <a class=" btn btn-sm btn-success pull-center view"
                                                       href="{{ route('revistas.show', $revista->id) }}"
                                                       title="Vizualizar"
                                                    >
                                                        <i class="not-active voyager-eye"></i>
                                                    </a>

                                                    @if($revista->vender && !in_array($revista->id, $minhaLista) )
                                                        {{ Form::open(['route' => ['lista.store'],'class'=>'form-horizontal', 'method' => 'POST']) }}

                                                        {{ csrf_field() }}

                                                        {!! Form::hidden('revista_id', $revista->id ?? null) !!}
                                                        <button title="Adicionar a Lista de desejos" class="btn btn-sm btn-primary pull-center"
                                                                type="submit"
                                                        >
                                                            <i class="not-active voyager-gift"></i>
                                                        </button>
                                                    @endif


                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6" align="center">Nenhum dado cadastrado até o momento</td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection