@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Revista</li>
    </ol>
@endsection

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-book"></i>{{ $revista->getTipo() .' : '. $revista->colecao->titulo->nome  .' nº '. $revista->numero_volume .' - '. $revista->sub_titulo }}
        </h1>
    </div>

    <div id="gradient_bg"></div>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">
                            <div class="form-group" align="center">
                                <h5 align="left">Preview: </h5>
                                <hr>
                                <img src="{{ $revista->imagem ?? asset('vendor/images/icon-no-image.png') }}"
                                     style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />
                                <p><strong> Nome: </strong>{{ $revista->sub_titulo }}</p>
                                <p><strong> Lançamento: </strong>{{ date('d/m/Y', strtotime($revista->lancamento)) }}</p>
                                <p><strong> Editora: </strong>{{ $revista->editora }}</p>
                                <p><strong> Obra: </strong>{{ $revista->colecao->titulo->nome }}</p>
                                <p><strong> Volume: </strong>{{ $revista->numero_volume }}</p>
                                <p><strong> Proprietário: </strong>{{ $revista->colecao->usuario->name }}</p>
                            </div>
                            <hr>

                            <div class="form-group">
                                <h5>Resumo da Obra:</h5>
                                <p>{{ $revista->colecao->titulo->resumo }}</p>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>


                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">

                            {{ Form::open(['route' => ['comentarios.store'],'class'=>'form-horizontal', 'method' => 'POST']) }}

                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <label for="descricao"><h5>Deixe seu comentário:</h5></label>
                                    {!! Form::hidden('revista', $revista->id ?? null) !!}
                                    {!! Form::textarea('descricao', '',['class'=>'form-control', 'rows' => 3,'placeholder'=>'Comentário' , 'required']) !!}
                                    <br>
                                    <button type="submit" class="btn btn-primary pull-right">
                                        <i class="voyager-paper-plane"></i>
                                        <span> Postar</span>
                                    </button>
                                </div>
                            </div>


                            <div>
                                <h5>Comentários: </h5>
                                <table class="table">
                                    <tbody>
                                    @forelse($comentarios as $comentario)
                                    <tr>
                                        <td width="80">
                                            <img src="{{ $comentario->usuario->avatar ?? asset('/vendor/images/user_default.png') }}"
                                                 class="avatar"
                                                 style="border-radius:50%; width:60px; height:60px; border:5px solid #fff;"
                                                 alt="{{ $comentario->usuario->name }} avatar">
                                        </td>
                                        <td style="text-align: left">
                                            <p><strong> {{ ucwords( $comentario->usuario->name) }} </strong></p>
                                            <p>"{{ $comentario->descricao }}"</p>
                                            <p class="text-info font-size-10">Comentado em: {{ $comentario->created_at->format('d/m/Y') ?? '' }}</p>
                                        </td>
                                        <td width="10">
                                            @if($comentario->usuario->id == auth()->user()->id)
                                                <button title="Delete" class="btn btn-sm btn-danger pull-center delete"
                                                        href="#"
                                                        data-action="{{ route('comentarios.destroy', $comentario->id) }}"
                                                        data-redirect="{{ route('revistas.show', $revista->id) }}"
                                                >
                                                    <i class="not-active voyager-trash"></i>
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td>Nenhum Comentário até o momento</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>

                            {{ $comentarios->links() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection