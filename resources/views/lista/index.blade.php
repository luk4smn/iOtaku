@extends('template.template')

@section('breacumbs')
    <ol class="breadcrumb hidden-xs">
        <li class="active">
            <a href="{{ route('home') }}">
                <i class="voyager-home" ></i> Principal
            </a>
        </li>
        <li>Lista de desejos</li>
    </ol>
@endsection

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-gift"></i>
            {{ 'Lista de desejos' }}
        </h1>

    </div>

    <div id="gradient_bg"></div>
@endsection

@section('content')
    <div class="page-content container-fluid">
        <div class="row">

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ HTML::ul($errors->all()) }}
                </div>
            @endif

            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <div class="panel-body">

                            <div class="form-group">
                                <div class="alert alert-info alert-dismissible fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <i class="voyager-info-circled">
                                        <span>{{ ' Se o proprietário desistir de fazer a venda do item, ele sairá automaticamente da sua lista.' }}</span>
                                    </i>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <table  class="table datatable-export" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Capa</th>
                                            <th>Revista</th>
                                            <th>Tipo</th>
                                            <th>Usuário Proprietário</th>
                                            <th>Ações</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($lista as $item)
                                            <tr>
                                                <td>{{ $item->id }}</td>
                                                <td>
                                                    <img src="{{ $item->revista->imagem  }}" alt="Miniatura" class="capa" style="width: 36px; height: 36px; border: 1px solid rgb(34,166,239);">
                                                </td>
                                                <td>
                                                    <strong>{{ $item->revista->colecao->titulo->nome  .' - '. $item->revista->numero_volume .' - '. $item->revista->sub_titulo }}</strong>
                                                </td>
                                                <td>{{ $item->revista->getTipo() }}</td>
                                                <td>{{ $item->revista->colecao->usuario->name }}</td>

                                                <td id="bread-actions" align="center">
                                                    <a class=" btn btn-sm btn-success pull-center view"
                                                       href="{{ route('revistas.show', $item->revista->id) }}"
                                                       title="Vizualizar Revista"
                                                    >
                                                        <i class="not-active voyager-eye"></i>
                                                    </a>

                                                    <a class=" btn btn-sm btn-dark pull-center view"
                                                       href="{{ route('users.show', $item->revista->colecao->usuario->id) }}"
                                                       title="Vizualizar Prorpietário"
                                                    >
                                                        <i class="not-active voyager-person"></i>
                                                    </a>

                                                    <button title="Delete" class="btn btn-sm btn-danger pull-center delete"
                                                            href="#"
                                                            data-action="{{ route('lista.destroy', $item->id) }}"
                                                            data-redirect="{{ route('lista.index') }}"
                                                    >
                                                        <i class="not-active voyager-trash"></i>
                                                    </button>

                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" align="center">Nenhum dado cadastrado até o momento</td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection