@push('scripts')

    @if (isset($errors) && count($errors) > 0)
        <script type="text/javascript">
            new swal({
                title: 'Erro:',
                text: "Ocorreram erros ao validar os dados",
                type: 'error',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
        </script>
    @endif


    @if (Illuminate\Support\Facades\Session::has('info'))
        <script type="text/javascript">
            new swal({
                title: 'Info:',
                text: "{{ Illuminate\Support\Facades\Session::get('info') }}",
                type: 'info',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
        </script>
    @endif

    @if (Illuminate\Support\Facades\Session::has('success'))
        <script type="text/javascript">
            new swal({
                title: 'Sucesso :',
                text: "{{ Illuminate\Support\Facades\Session::get('success') }}",
                type: 'success',
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
        </script>
    @endif

@endpush
