<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 25/11/18
 * Time: 20:00
 */

use Illuminate\Database\Seeder;

class TitulosSeeder extends Seeder
{

    public function run()
    {
        \App\Entities\Titulo::create([
            'nome' => 'Naruto',
            'autor' => 'Masashi Kishimoto',
            'resumo' => 'Conta a história de Naruto Uzumaki, um jovem ninja que constantemente procura por reconhecimento  e sonha em se tornar Hokage, o ninja líder de sua vila.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'One Piece',
            'autor' => 'Eiichiro Oda',
            'resumo' => 'One Piece conta as aventuras de Monkey D. Luffy, um jovem cujo corpo ganhou as propriedades de borracha após ter comido uma fruta do Diabo acidentalmente.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'Dragon Ball',
            'autor' => 'Akira Toriyama',
            'resumo' => 'Um menino com rabo de macaco chamado Son Goku fazendo amizade com uma adolescente chamada Bulma, que ele acompanha para encontrar as sete Esferas do Dragão, que convocam o dragão Shenlong para  conceder ao usuário um desejo.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'Golgo 13',
            'autor' => 'Takao Saito',
            'resumo' => 'Duke Togo, o protagonista do mangá é um assassino de aluguel seu nome deriva do Gólgota, a colina onde Jesus foi crucificado.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'Boku no Hero Academia',
            'autor' => 'Kōhei Horikoshi',
            'resumo' => 'Boku no Hero Academia, também conhecido como My Hero Academia no ocidente, é uma série de mangá escrita e ilustrada por Kōhei Horikoshi. Os capítulos do mangá são publicados na revista Weekly Shōnen Jump desde julho de 2014, e, até o momento, foram compilados em 24 volumes em formato tankōbon pela editora Shueisha.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'Death Note',
            'autor' => 'Tsugumi Ohba',
            'resumo' => 'Death Note é uma série de mangá escrita por Tsugumi Ohba e ilustrada por Takeshi Obata. Os capítulos do mangá foram serializados na revista semanal japonesa Weekly Shōnen Jump de 2003 até 2006, com os capítulos compilados em um total de 12 volumes tankōbon e lançados pela editora Shueisha.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'Tokyo Ghoul',
            'autor' => 'Sui Ishida',
            'resumo' => 'Tokyo Ghoul é o título de uma série de mangá escrito e ilustrado por Sui Ishida. O primeiro capítulo do mangá foi lançado em 8 de setembro de 2011 na revista semanal Young Jump e o último em 18 de setembro de 2014.',
        ]);

        \App\Entities\Titulo::create([
            'nome' => 'Full Metal Alchemist',
            'autor' => 'Hiromu Arakawa',
            'resumo' => 'Fullmetal Alchemist é um anime de mangá shōnen escrita e ilustrada por Hiromu Arakawa. O mangá foi serializado na revista mensal japonesa Monthly Shōnen Gangan entre agosto de 2001 e junho de 2010, com os seus 108 capítulos individuais compilados em 27 volumes em formato tankōbon e publicados pela editora Square Enix.',
        ]);

        $faker = \Faker\Factory::create('en_US');
		
        $faker1 = \Faker\Factory::create('ja_JP');

        for($i = 0; $i < 15; $i++){
            \App\Entities\Titulo::create([
                'nome' => $faker->name,
                'autor' => $faker1->name,
                'resumo' => 'Tradicional Manhwa',
            ]);

        }
    }

}