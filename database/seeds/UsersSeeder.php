<?php


use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run(){
        \App\User::create([
            'name'  => 'Lucas Nunes',
            'email' => 'lucas.mn@outlook.com',
            'password' => bcrypt('2005*1994')
        ]);

        factory(App\User::class, 500)->create();
    }

}