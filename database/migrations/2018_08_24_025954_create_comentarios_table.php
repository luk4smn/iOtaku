<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descricao');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('revista_id');
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('usuario_id')
                ->references('id')
                ->on('users');

            $table
                ->foreign('revista_id')
                ->references('id')
                ->on('revistas');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
