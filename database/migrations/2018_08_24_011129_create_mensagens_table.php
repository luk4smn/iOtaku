<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensagens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('usuario_remetente_id');
            $table->unsignedBigInteger('usuario_destinatario_id');
            $table->mediumText('mensagem');
            $table->boolean('lido')->default(false);
            $table->string('conversa_id');
            $table->timestamps();
            $table->softDeletes();

            $table->index('conversa_id');

            $table
                ->foreign('usuario_remetente_id')
                ->references('id')
                ->on('users');

            $table
                ->foreign('usuario_destinatario_id')
                ->references('id')
                ->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensagens');
    }
}
