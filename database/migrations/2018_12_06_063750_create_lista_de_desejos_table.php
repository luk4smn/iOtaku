<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListaDeDesejosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lista_de_desejos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('revista_id');
            $table->unsignedBigInteger('usuario_id');

            $table
                ->foreign('revista_id')
                ->references('id')
                ->on('revistas');

            $table
                ->foreign('usuario_id')
                ->references('id')
                ->on('users');


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lista_de_desejos');
    }
}
