<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprestimosItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprestimos_itens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('revista_id');
            $table->unsignedBigInteger('emprestimo_id');

            $table
                ->foreign('revista_id')
                ->references('id')
                ->on('revistas');

            $table
                ->foreign('emprestimo_id')
                ->references('id')
                ->on('emprestimos');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprestimos_itens');
    }
}
