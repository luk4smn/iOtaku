<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmprestimosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprestimos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumText('descricao');
            $table->dateTime('data_emprestimo');
            $table->dateTime('data_devolucao')->nullable();
            $table->unsignedBigInteger('amigo_id');
            $table->unsignedBigInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('usuario_id')
                ->references('id')
                ->on('users');

            $table
                ->foreign('amigo_id')
                ->references('id')
                ->on('amigos');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprestimo');
    }
}
