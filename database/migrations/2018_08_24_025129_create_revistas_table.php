<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revistas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('colecao_id');
            $table->string('imagem')->nullable();
            $table->string('sub_titulo');
            $table->date('lancamento');
            $table->string('editora');
            $table->integer('numero_volume');
            $table->integer('tipo');
            $table->boolean('vender')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('colecao_id')
                ->references('id')
                ->on('colecoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revistas');
    }
}
