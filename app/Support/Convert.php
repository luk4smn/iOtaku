<?php

namespace App\Support;

use Carbon\Carbon;

class Convert
{
    public static function moneyToDecimal($money){
        return self::getfloat($money,'R$ ');
    }

    public static function decimalToMoney($decimal){
        try {
            return number_format($decimal, 2, ',', '.');
        } catch (\Exception $exception) {
            return $decimal;
        }
    }

    public static function dateToDBFormat($brDate){
        try {
            if(empty($brDate)){
                return null;
            }else{
                return Carbon::createFromFormat("d/m/Y", trim($brDate))->toDateString();
            }
        } catch (\Exception $e) {
            return $brDate;
        }
    }

    public static function DBToCarbonFormat($bdDate){
        try {
            if (strstr($bdDate, '-'))
                return Carbon::parse($bdDate);

            return $bdDate;
        } catch (\Exception $e) {
            return $bdDate;
        }
    }

    public static function DBDateTimeToStringFormat($bdDate){
        try{
            if(strstr($bdDate, '-'))
                return Carbon::parse($bdDate)->format('d/m/Y H:i:s');

            if(strstr($bdDate, '/'))
                return Carbon::createFromFormat('d/m/Y H:i:s', $bdDate);

            return $bdDate;
        } catch (\Exception $e) {
            return $bdDate;
        }
    }

    public static function DBDateToStringFormat($bdDate){
        try{
            if(strstr($bdDate, '-'))
                return Carbon::parse($bdDate)->format('d/m/Y');

            return $bdDate;
        } catch (\Exception $e) {
            return $bdDate;
        }
    }

    public static function dateTimeToDBFormat($brDateTime){
        try{
            if(preg_match("/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/", trim($brDateTime))){
                return Carbon::createFromFormat("d/m/Y H:i:s", trim($brDateTime))->toDateTimeString();
            } else if (preg_match("/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/", trim($brDateTime))){
                return Carbon::createFromFormat("d/m/Y H:i", trim($brDateTime))->toDateTimeString();
            } return $brDateTime;
        } catch (\Exception $e){
            return $brDateTime;
        }
    }


    public static function validateBRDate($value){
        try{

            $br_date = self::dateToDBFormat($value);

            $datetime = new \DateTime();

            $isValid = $datetime->modify($br_date);

            if($isValid->format('d/m/Y') === $value)
                return true;

        } catch (\Exception $e){
            return false;
        }

        return false;
    }

    public static function validateBRDateTime($value){
        try{
            $br_datetime = self::dateTimeToDBFormat($value);

            $datetime = new \DateTime();

            $isValid = $datetime->modify($br_datetime);

            if($isValid->format('Y-m-d H:i:s') === $value)
                return true;

        } catch (\Exception $e){
            return false;
        }

        return false;
    }

    function convertBytes($size){

        $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");

        return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
    }


    function removeAllSpecialCaractersAndPreserveSpace($string)
    {
        $normalizeChars = array(
            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
            'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
        );

        $string = strtr( $string, $normalizeChars );

        return $string = preg_replace('/[^a-zA-Z0-9 ]/', '', $string);
    }


    public static function getfloat($str,$unity){
        $str = str_replace($unity, '', $str);

        if (strstr($str, ",")) {
            $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
            $str = str_replace(",", ".", $str); // replace ',' with '.'
        }

        if (preg_match('#([0-9\.]+-)#', $str, $match)) { // search for number that may contain '.'
            return floatval($match[0]);
        } else {
            return floatval($str); // take some last chances with floatval
        }
    }
}
