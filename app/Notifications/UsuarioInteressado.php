<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 06/12/18
 * Time: 12:08
 */

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class UsuarioInteressado extends Notification implements ShouldQueue
{
    use Queueable;

    public $model;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('iOtaku - Interesse no seu item a venda')
            ->greeting("Olá, $notifiable->name!")
            ->line('Você recebeu esse email pois o seu item que está marcado para venda foi marcada como interesse de um usuário do sistema.')
            ->line('O usuário interessado é :' .$this->model->usuario->name)
            ->line('Para mais detalhes, acesse o sistema.')
            ->action('Logar no sistema', url('/login'))
            ->with('Atenciosamente,')
            ->salutation('iOtaku.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

}