<?php

namespace App;

use App\Entities\Amigo;
use App\Entities\Colecao;
use App\Entities\Comentario;
use App\Entities\Emprestimo;
use App\Entities\ListaDeDesejos;
use App\Entities\Mensagem;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use App\Notifications\ResetPassword as ResetPasswordNotification;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function mensagensEnviadas(){
        return $this->hasMany(Mensagem::class, "usuario_remetente_id");
    }

    public function mensagensRecebidas(){
        return $this->hasMany(Mensagem::class, "usuario_destinatario_id");
    }

    public function emprestimos(){
        return $this->hasMany(Emprestimo::class, "usuario_id");
    }

    public function emprestimosPendentes(){
        return $this->hasMany(Emprestimo::class, "usuario_id")
            ->whereNull('data_devolucao');
    }

    public function colecoes(){
        return $this->hasMany(Colecao::class, "usuario_id");
    }

    public function amigos(){
        return $this->hasMany(Amigo::class, "usuario_id");
    }

    public function comentarios(){
        return $this->hasMany(Comentario::class, "usuario_id");
    }

    public function listaDeDesejos(){
        return $this->hasMany(ListaDeDesejos::class, "usuario_id");
    }

    public function setAvatarImage($filePath)
    {
        $filePath = $filePath->getRealPath();
        $file = file_get_contents($filePath);

        $fileName = 'profile/user_' . $this->id . "/avatar.jpg";

        if(!empty($this->attributes['avatar']) && Storage::exists($this->attributes['avatar']))
            Storage::disk('public')
                ->delete($this->attributes['avatar']);

        Storage::disk('public')
            ->put($fileName, $file);

        $this->avatar = $fileName;
        $this->save();

    }

    public function getAvatarAttribute()
    {
        if($this->attributes['avatar'] && !str_contains($this->attributes['avatar'], 'http'))
            return Storage::disk('public')
                ->url($this->attributes['avatar']);

        return $this->attributes['avatar'];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


}
