<?php

namespace App\Observers;


class Observer
{
    public function saved($model){}

    public function created($model){}

    public function updated($model){}

    public function deleted($model){}
}
