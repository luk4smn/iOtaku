<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 06/12/18
 * Time: 12:28
 */

namespace App\Observers;


use App\Notifications\UsuarioInteressado as UsuarioInteressadoNotification;

class ListaDeDesejosObserver extends Observer
{
    public function created($model){

        $model
            ->revista
            ->colecao
            ->usuario
            ->notify(new UsuarioInteressadoNotification($model));
    }

}