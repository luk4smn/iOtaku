<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 29/11/18
 * Time: 01:02
 */

namespace App\Providers;



use App\Entities\ListaDeDesejos;
use App\Observers\ListaDeDesejosObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{

    public function boot()
    {
        ListaDeDesejos::observe(ListaDeDesejosObserver::class);
    }

}
