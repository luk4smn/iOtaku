<?php


namespace App\Entities;

use App\Criterias\CriteriaInterface;
use App\Support\Convert;
use App\Traits\Searchable;
use App\Transformers\Transformer;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Datatables\Html\Builder;

class Entity extends Model
{
    use Searchable, SoftDeletes;


    public static function syncHasManyRelation($model, $relation, $field, $array){
        $fill = collect($array ?? [])->map(function ($item) {
            return $item;
        })->toArray();

        $model->{$relation}()->whereIn($field, array_pluck($array ?? [], $field))->delete();

        $model->{$relation}()->createMany($fill);

        $model->{$relation}()->whereNotIn($field, array_pluck($array ?? [], $field))->delete();
    }

}