<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 20:08
 */

namespace App\Entities;


use App\User;

class Emprestimo extends Entity
{
    protected $table = 'emprestimos';

    protected $fillable = [
        'revista_id',
        'amigo_id',
        'descricao',
        'data_emprestimo',
        'data_devolucao',
        'usuario_id'
    ];

    protected $dates = [
        'data_emprestimo',
        'data_devolucao',
    ];

    public function itens(){
        return $this->HasMany(EmprestimoItem::class, "emprestimo_id");
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function amigo(){
        return $this->belongsTo(Amigo::class, "amigo_id");
    }

    public function getDataEmprestimoAttribute()
    {
        $time = strtotime($this->attributes['data_emprestimo']);

        return date('Y-m-d', $time);
    }

    public function getDataDevolucaoAttribute()
    {
        if($this->attributes['data_devolucao']){
            $time = strtotime($this->attributes['data_devolucao']);

            return date('Y-m-d', $time);
        }

    }

}