<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 20:08
 */

namespace App\Entities;


use App\User;

class Comentario extends Entity
{

    protected $table = 'comentarios';

    protected $fillable = [
        'descricao',
        'usuario_id',
        'revista_id',
    ];

    public function revista(){
        return $this->belongsTo(Revista::class, "revista_id");
    }

    public function usuario(){
        return $this->belongsTo(User::class, "usuario_id");
    }
}