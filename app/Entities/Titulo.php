<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 22:28
 */

namespace App\Entities;


class Titulo extends Entity
{
    protected $table = 'titulos';

    protected $fillable = [
        'nome',
        'autor',
        'resumo',
    ];

    public function colecoes(){
        return $this->hasMany(Colecao::class, "titulo_id");
    }
}