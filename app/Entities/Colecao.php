<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 20:07
 */

namespace App\Entities;


use App\User;

class Colecao extends Entity
{
    protected $table = "colecoes";

    protected $fillable = [
        'total_volumes',
        'usuario_id',
        'titulo_id'
    ];

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function titulo(){
        return $this->belongsTo(Titulo::class, 'titulo_id');
    }

    public function itens(){
        return $this->hasMany(Revista::class, 'colecao_id');
    }

}