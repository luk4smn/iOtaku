<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 29/11/18
 * Time: 04:01
 */

namespace App\Entities;


class EmprestimoItem extends Entity
{
    protected $table = 'emprestimos_itens';

    protected $fillable = [
        'revista_id',
        'emprestimo_id',
    ];

    public function revista(){
        return $this->belongsTo(Revista::class, "revista_id");
    }

    public function emprestimo(){
        return $this->belongsTo(Emprestimo::class, 'emprestimo_id');
    }

}