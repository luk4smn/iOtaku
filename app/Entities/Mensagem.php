<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 20:07
 */

namespace App\Entities;


use App\User;

class Mensagem extends Entity
{
    protected $table = "mensagens";

    protected $fillable = [
        'usuario_remetente_id',
        'usuario_destinatario_id',
        'mensagem',
        'conversa_id',
        'lido'
        ];

    public function remetente(){
        return $this->belongsTo(User::class, 'usuario_remetente_id');
    }

    public function destinatario(){
        return $this->belongsTo(User::class, "usuario_destinatario_id");
    }
}