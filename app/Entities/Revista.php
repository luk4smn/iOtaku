<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 20:08
 */

namespace App\Entities;


use Illuminate\Support\Facades\Storage;

/**
 * @property mixed data_lancamento
 */
class Revista extends Entity
{

    protected $table = 'revistas';

    protected $fillable = [
        'imagem',
        'sub_titulo',
        'lancamento',
        'editora',
        'numero_volume',
        'tipo',
        'colecao_id',
        'vender'
    ];

    /** @var array - used to set formats on dates */
    protected $dates = [
        'lancamento'
    ];

    const
        MANGA   = 1,
        MANHWAS = 2;

    public function comentarios(){
        return $this->hasMany(Comentario::class, "revista_id");
    }

    public function colecao(){
        return $this->belongsTo(Colecao::class, "colecao_id");
    }

    public function emprestimos(){
        return $this->hasMany(EmprestimoItem::class, "revista_id");
    }

    public function getTipo()
    {
        if($this->tipo == self::MANGA){
            return "Mangá";
        }
        else if($this->tipo == self::MANGA){
            return "Manhwa";
        }
        return null;
    }

    public function isFree()
    {
        $emprestimoItem = $this->emprestimos
            ->flatMap(function ($item){
                return $item->emprestimo
                    ->where('data_devolucao', null)
                    ->get();
            });

        return $emprestimoItem->count() ? false : true;
    }

    public function setImage($filePath)
    {
        $filePath = $filePath->getRealPath();
        $file = file_get_contents($filePath);

        $fileName = 'revista/' . $this->id . "/image.jpg";

        if(!empty($this->attributes['imagem']) && Storage::exists($this->attributes['imagem']))
            Storage::disk('public')
                ->delete($this->attributes['imagem']);

        Storage::disk('public')
            ->put($fileName, $file);

        $this->imagem = $fileName;
        $this->save();
    }

    public function getImagemAttribute()
    {
        if($this->attributes['imagem'] && !str_contains($this->attributes['imagem'], 'http'))
            return Storage::disk('public')
                ->url($this->attributes['imagem']);

        return $this->attributes['imagem'];
    }

    public function getLancamentoAttribute()
    {
        $time = strtotime($this->attributes['lancamento']);

        return date('Y-m-d', $time);
    }

}