<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 23/08/18
 * Time: 20:08
 */

namespace App\Entities;


use App\User;

class Amigo extends Entity
{
    protected $table = 'amigos';

    protected $fillable = [
        'nome',
        'endereco',
        'telefone',
        'email',
        'usuario_id'
    ];

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function emprestimos(){
        return $this->hasMany(Emprestimo::class, "amigo_id");
    }

}