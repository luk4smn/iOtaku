<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 06/12/18
 * Time: 03:33
 */

namespace App\Entities;


use App\User;

class ListaDeDesejos extends Entity
{

    protected $table = 'lista_de_desejos';

    protected $fillable = [
        'revista_id',
        'usuario_id',
    ];

    public function revista(){
        return $this->belongsTo(Revista::class, "revista_id");
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    // verifica se item já está na lista de desejos ou se eu já sou o dono desse mangá.
    public static function verifyItem($id)
    {
        $revista = Revista::findOrfail($id);

        if($revista->colecao->usuario->id == auth()->user()->id){
            return true;
        }

        if(auth()->user()->listaDeDesejos->where('revista_id', $revista->id)->count())
            return true;
        else
            return false;
    }

}