<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 29/11/18
 * Time: 00:05
 */

namespace App\Http\Requests\Amigo;


use Illuminate\Foundation\Http\FormRequest;

class StoreAmigoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'      => 'required',
            'endereco'  => 'required',
            'telefone'  => 'required|min:6',
            'email'     => 'required|email',
        ];
    }


}