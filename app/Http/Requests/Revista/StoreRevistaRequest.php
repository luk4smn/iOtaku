<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 27/11/18
 * Time: 05:53
 */

namespace App\Http\Requests\Revista;


use Illuminate\Foundation\Http\FormRequest;

class StoreRevistaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'numero_volume' => 'required|gt:0',
            'tipo'          => 'required',
            'colecao_id'    => 'required',
            'editora'       => 'required',
            'lancamento'    => 'required|date',
            'imagem'        => 'nullable',
            'sub_titulo'    => 'required',
        ];
    }

}