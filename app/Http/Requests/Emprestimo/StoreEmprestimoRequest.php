<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 29/11/18
 * Time: 03:12
 */

namespace App\Http\Requests\Emprestimo;


use Illuminate\Foundation\Http\FormRequest;

class StoreEmprestimoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'revistas'  => 'nullable',
            'amigo_id'  => 'required',
            'descricao'  => 'required',
            'data_emprestimo'   => 'required|date',
            'data_devolucao'    => 'nullable|date'
        ];
    }


}