<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 26/10/18
 * Time: 16:03
 */

namespace App\Http\Requests\User;


use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
//            'telefone' => [
//                'required',
//                'numeric',
//                new TelefoneRule,
//                'unique:users_telefones,telefone'
//            ],
//            'tipo_telefone' => [
//                'required',
//                Rule::in([ Telefone::TIPO_CELULAR, Telefone::TIPO_FIXO ])
//            ],
//            'tipo_confirmacao' => [
//                'required',
//                Rule::in([ Telefone::TIPO_CONFIRMACAO_SMS, Telefone::TIPO_CONFIRMACAO_VOZ ])
//            ]
            'name'      => 'required',
            'email'     => 'required|string|email|max:255|unique:users,email,' . auth()->id(),
            'avatar'    => 'nullable|image',
            'password'  => 'confirmed',
        ];
    }


}