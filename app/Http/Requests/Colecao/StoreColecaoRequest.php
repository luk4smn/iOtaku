<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 25/11/18
 * Time: 21:40
 */

namespace App\Http\Requests\Colecao;


use Illuminate\Foundation\Http\FormRequest;

class StoreColecaoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'total_volumes'  => 'required|gt:0',
            'titulo_id'      => 'required',
        ];
    }

}