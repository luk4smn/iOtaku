<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 06/12/18
 * Time: 03:44
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class ListaDeDesejosController extends Controller
{
    public function index()
    {
        $lista = auth()->user()->listaDeDesejos;

        return view('lista.index' ,compact('lista'));
    }

    public function store(Request $request)
    {
        $revista        = $request->input('revista_id');
        $ajaxRequest    = $request->input('ajax');

        if($revista){
            auth()->user()->listaDeDesejos()->create(['revista_id' => $revista]);

            if($ajaxRequest){
                return [
                    'data' => [
                        'status' => 'success',
                        'message' => 'Item adicionado a lista com sucesso!',
                        'info'    => 'Recarregando em 2 segundos...',
                        'redirect' => route('search.index'),
                        'code' => 204
                    ]
                ];
            }

            return redirect()->back()->with('success', 'Item adicionado a lista com sucesso');
        }

        return redirect()->back()->withErrors('Erro ao salvar');
    }

    public function destroy($id){
        $item = auth()->user()->listaDeDesejos->find($id);

        if($item){
            $item->delete();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Excluído com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];

        }


        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível excluir !',
                'info'    => 'Esse item não está atrelado ao seu usuário',
                'code' => 401
            ]
        ];
    }



}