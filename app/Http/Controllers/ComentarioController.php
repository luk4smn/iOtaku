<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 04/12/18
 * Time: 12:59
 */

namespace App\Http\Controllers;


use App\Entities\Comentario;
use App\Entities\Revista;
use Illuminate\Http\Request;

class ComentarioController extends Controller
{

    public function store(Request $request)
    {
        $revista = $request->input('revista');
        $descricao = $request->input('descricao');

        if($revista){

            $revista = Revista::findOrFail($revista);
            $revista->comentarios()->create([
                'descricao' => $descricao,
                'usuario_id' => auth()->user()->id
            ]);

            return redirect()->back()->with('success', 'Seu comentário foi salvo');
        }

        alert()->error('Erro !','ocorreu um erro ao inserir');
        return redirect()->back()->withErrors('Erro ao salvar');
    }


    public function destroy($id)
    {
        $comentario = Comentario::findOrFail($id);

        if($comentario->usuario->id == auth()->user()->id)
        {
            $comentario->delete();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Excluído com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível excluir !',
                'info'    => 'Existem empréstimos em abrerto vinculados a essa coleção',
                'code' => 401
            ]
        ];
    }


}