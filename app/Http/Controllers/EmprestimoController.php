<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 27/11/18
 * Time: 04:31
 */

namespace App\Http\Controllers;


use App\Entities\Emprestimo;
use App\Http\Requests\Emprestimo\StoreEmprestimoRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmprestimoController extends Controller
{
    public function index(Request $request)
    {
        $pendentes = $request->input('pendentes');

        if($pendentes){
            $emprestimos = auth()->user()
                ->emprestimos
                ->where('data_devolucao', null);

            return view('emprestimo.index', compact('emprestimos', 'pendentes'));
        }

        $emprestimos = auth()->user()->emprestimos;

        return view('emprestimo.index', compact('emprestimos'));
    }

    public function devolver(Emprestimo $emprestimo)
    {
        if($emprestimo->usuario->id == auth()->user()->id){

            $emprestimo->data_devolucao = Carbon::now();

            $emprestimo->save();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Devolução realizada com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        };

    }

    public function edit($id)
    {
        $emprestimo = auth()->user()->emprestimos->find($id);

        if(isset($emprestimo))
            return $emprestimo;
    }

    public function store(StoreEmprestimoRequest $request)
    {
        $revistas = $request->input('revistas');

        $emprestimo  = auth()->user()->emprestimos()->create([
            'descricao' => $request['descricao'],
            'amigo_id' => $request['amigo_id'],
            'data_emprestimo' => $request['data_emprestimo'],
        ]);

        foreach ($revistas as $revistaId){
            $emprestimo->itens()->create([
                'revista_id' => $revistaId
            ]);
        }

        alert()->success('OK !','Dados inseridos com sucesso');
        return redirect()->route('emprestimos.index');
    }

    public function update($id, StoreEmprestimoRequest $request)
    {
//        $revistas = $request->input('revistas');

        $emprestimo = auth()->user()->emprestimos->find($id);

        $emprestimo->update([
            'descricao' => $request['descricao'],
            'amigo_id' => $request['amigo_id'],
            'data_emprestimo' => $request['data_emprestimo'],
        ]);

       /* foreach ($revistas as $revistaId){
            $emprestimo->itens->updateOrCreate([
                'emprestimo_id' => $emprestimo->id,
            ],[
                'revista_id'   => $revistaId,
            ]);
        }*/

        alert()->success('OK !','Dados Atualizados com sucesso');

        return redirect()->route('emprestimos.index');
    }

    public function destroy($id)
    {
        $emprestimo = auth()->user()->emprestimos->find($id);

        if($emprestimo)
        {
            $emprestimo->delete();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Excluído com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

    }

}