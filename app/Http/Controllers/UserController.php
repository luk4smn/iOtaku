<?php


namespace App\Http\Controllers;


use App\Http\Requests\User\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = app(User::class);
    }

    public function list(Request $request)
    {
        $search = $request->input('search');

        if($search)
        {
            return $this->model->where('name', 'like', "%$search%")
                ->get();
        }

        return back()->withErrors('acesso negado');
    }

    public function show(User $user)
    {
        if($user->id == auth()->user()->id){
            return view('profile.index');
        }
        return view('find_user.index', compact('user'));
    }

    public function update(UpdateUserRequest $request)
    {
        $user = auth()->user();

        $user->fill(array_except(
            $request->validated(), ['avatar', 'password']
        ));

        if($password = $request->input('password')){
            $user->password = bcrypt($password);
        }

        if($request->has('avatar')){
            $user->setAvatarImage($request['avatar']);
        }

        $user->save();

        alert()->success('OK !','Dados Atualizados com sucesso');
        return redirect()->route('myprofile.index');
    }

    public function myProfile()
    {
        return view('profile.index');
    }

    public function myProfileEdit()
    {
        return view('profile.edit');
    }

}