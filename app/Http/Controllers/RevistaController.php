<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 25/11/18
 * Time: 23:46
 */

namespace App\Http\Controllers;


use App\Entities\ListaDeDesejos;
use App\Entities\Revista;
use App\Http\Requests\Revista\StoreRevistaRequest;
use Illuminate\Http\Request;


class RevistaController extends Controller
{

    public function list(Request $request)
    {
        $search = $request->input('search');
        $disponivel = $request->input('disponivel');

        if($search)
        {
            $revistas = Revista::where('sub_titulo', 'like', "%$search%")
                ->get()
                ->each(function ($revista){
                    $revista->inList = ListaDeDesejos::verifyItem($revista->id);
                });
        }
        elseif($disponivel)
        {
            $filter = auth()->user()->emprestimos()
                ->whereNull('data_devolucao')
                ->get()
                ->flatMap(function ($emprestimo )
                {
                    return $emprestimo->itens->pluck('revista_id');
                });

            $itens = auth()->user()->colecoes->flatMap(
                function($colecao) use ($filter){
                    {
                        return $colecao->itens->whereNotIn('id', $filter);
                    }
                });

            $revistas  = $itens->each(
                function ($revista){
                    return $revista->sub_titulo = $revista->colecao->titulo->nome  .' - '. $revista->numero_volume .' - '. $revista->sub_titulo;
                })
                ->pluck('sub_titulo', 'id');
        }
        else
        {
            $revistas = null;
        }

        return $revistas;
    }


    public function show($id)
    {
        $revista = Revista::findOrFail($id);

        $comentarios = $revista->comentarios()->paginate();

        return view('revistas.show', compact('revista', 'comentarios'));
    }

    public function edit($id)
    {
        $revista = Revista::findOrFail($id);

        $count = auth()->user()->colecoes->find($revista->colecao_id);

        if(isset($count))
            return $revista;
    }

    public function store(StoreRevistaRequest $request)
    {
        $revista = Revista::create(array_except(
            $request->validated(), ['imagem']
        ));

        if($request->has('imagem')){
            $revista->setImage($request['imagem']);
        }

        $revista->save();

        alert()->success('OK !','Dados inseridos com sucesso');
        return redirect()->route('colecoes.itens.list', $revista->colecao_id);
    }

    public function update($id, StoreRevistaRequest $request)
    {
        $revista = Revista::findOrFail($id);

        if($revista){
            $revista->fill(array_except(
                $request->validated(), ['imagem']
            ));

            if($request->has('imagem')){
                $revista->setImage($request['imagem']);
            }

            $revista->save();

            alert()->success('OK !','Dados Atualizados com sucesso');
        }
        else{
            alert()->error('Erro !','Dado não localizado');
        }

        return redirect()->route('colecoes.itens.list', $revista->colecao_id);
    }

    public function destroy($id)
    {
        $revista = Revista::findOrFail($id);

        $colecaoDoUsuario = auth()->user()->colecoes->find($revista->colecao_id);

        if($revista->isFree() && isset($colecaoDoUsuario))
        {
            ListaDeDesejos::where('revista_id',$revista->id)
                ->get()
                ->each(function ($lista){
                    $lista->delete();
                });

            $revista->delete();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Excluído com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível excluir !',
                'info'    => 'Existem empréstimos em abrerto vinculados a essa coleção',
                'code' => 401
            ]
        ];
    }

    public function marcarParaVenda(Revista $revista)
    {
        $colecaoDoUsuario = auth()->user()->colecoes->find($revista->colecao_id);

        if($colecaoDoUsuario){

            $revista->vender = true;

            $revista->save();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Item marcado para venda com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível marcar o item para venda !',
                'info'    => 'O usuário não é o dono dessa revista',
                'code' => 401
            ]
        ];
    }

    public function desmarcarParaVenda(Revista $revista)
    {
        $colecaoDoUsuario = auth()->user()->colecoes->find($revista->colecao_id);

        if($colecaoDoUsuario){

            ListaDeDesejos::where('revista_id',$revista->id)
                ->get()
                ->each(function ($lista){
                    $lista->delete();
                });

            $revista->vender = false;

            $revista->save();

            return [
                'data' => [
                    'status' => 'success',
                    'message' => 'Item desmarcado para venda com sucesso!',
                    'info'    => 'Recarregando em 2 segundos...',
                    'code' => 204
                ]
            ];
        }

        return [
            'data' => [
                'status' => 'error',
                'message' => 'Não é possível desmarcar o item para venda !',
                'info'    => 'O usuário não é o dono dessa revista',
                'code' => 401
            ]
        ];
    }


    public function tipos()
    {
        return $tipos = [
            1 => 'Mangá',
            2 => 'Manhwha'
        ];
    }


}