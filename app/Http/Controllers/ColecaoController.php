<?php


namespace App\Http\Controllers;


use App\Entities\Colecao;
use App\Http\Requests\Colecao\StoreColecaoRequest;
use App\User;
use Illuminate\Http\Request;

class ColecaoController
{

    public function index(Request $request)
    {
        if($request->input('user_id')){

            $user = User::findOrFail($request->input('user_id'));

            $colecoes = Colecao::where('usuario_id', $user->id)
                ->get();

            $nome = $user->name;

            return view('colecoes.another_user_index', compact('colecoes', 'nome'));
        }

        $colecoes = auth()->user()->colecoes;

        return view('colecoes.index', compact('colecoes'));
    }

    public function edit($id){
        return auth()->user()->colecoes->find($id);
    }

    public function store(StoreColecaoRequest $request){
        auth()->user()->colecoes()->create(
            $request->validated()
        );

        alert()->success('OK !','Coleção inserida com sucesso');
        return redirect()->route('colecoes.index');
    }

    public function update($id, StoreColecaoRequest $request){
        $colecao = auth()->user()->colecoes->find($id);

        $colecao->update(
            $request->validated()
        );

        alert()->success('OK !','Coleção alterada com sucesso');
        return redirect()->route('colecoes.index');
    }

    public function destroy($id){
        $colecao = auth()->user()->colecoes->find($id);

        if($colecao->itens->count()){
            return [
                'data' => [
                    'status' => 'error',
                    'message' => 'Não é possível excluir !',
                    'info'    => 'Existem itens vinculados a essa coleção',
                    'code' => 401
                ]
            ];
        }

        $colecao->delete();

        return [
            'data' => [
                'status' => 'success',
                'message' => 'Excluído com sucesso!',
                'info'    => 'Recarregando em 2 segundos...',
                'code' => 204
            ]
        ];
    }

    public function listItens(Colecao $colecao){
        $itens = $colecao->itens;

        if($colecao->usuario_id == auth()->user()->id){

            return view('revistas.index', compact('itens','colecao'));
        }

        $minhaLista = auth()->user()
            ->listaDeDesejos
            ->pluck('revista_id')
            ->toArray();

        return view('revistas.another_user_index', compact('itens','colecao', 'minhaLista'));
    }

}