<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 27/11/18
 * Time: 04:31
 */

namespace App\Http\Controllers;


use App\Http\Requests\Amigo\StoreAmigoRequest;

class AmigoController extends Controller
{

    public function index()
    {
        $amigos = auth()->user()->amigos;

        return view('amigo.index', compact('amigos'));
    }

    public function edit($id){
        return auth()->user()->amigos->find($id);
    }

    public function list()
    {
        return auth()->user()->amigos->pluck('nome','id');
    }

    public function store(StoreAmigoRequest $request)
    {
        auth()->user()->amigos()->create(
            $request->validated()
        );

        alert()->success('OK !','Amigo cadastrado com sucesso');
        return redirect()->route('amigos.index');
    }

    public function update($id, StoreAmigoRequest $request)
    {
        $amigo = auth()->user()->amigos->find($id);

        $amigo->update(
            $request->validated()
        );

        alert()->success('OK !','Amigo alterado com sucesso');
        return redirect()->route('amigos.index');
    }

    public function destroy($id)
    {
        $amigo = auth()->user()->amigos->find($id);

        $pendencia = auth()
            ->user()
            ->emprestimosPendentes
            ->where('amigo_id', $amigo->id);

        if($pendencia->count())
        {
            return [
                'data' => [
                    'status' => 'error',
                    'message' => 'Não é possível excluir !',
                    'info'    => 'Existem empréstimos em abrerto vinculados a esse amigo',
                    'code' => 401
                ]
            ];
        }

        $amigo->delete();

        return [
            'data' => [
                'status' => 'success',
                'message' => 'Excluído com sucesso!',
                'info'    => 'Recarregando em 2 segundos...',
                'code' => 204
            ]
        ];
    }

}