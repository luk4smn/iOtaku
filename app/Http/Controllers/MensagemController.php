<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 28/11/18
 * Time: 00:18
 */

namespace App\Http\Controllers;


use App\Entities\Mensagem;
use Illuminate\Http\Request;

class MensagemController extends Controller
{

    public function index()
    {
        $mensagens = Mensagem::where('usuario_remetente_id', auth()->user()->id)
            ->orWhere('usuario_destinatario_id', auth()->user()->id)
            ->orderBy('id','desc')
            ->get();

        $conversas = [];

        foreach($mensagens as $key => $mensagem){
            $conversas[$mensagem->conversa_id][$key] = $mensagem;
        }

        return view('mensagem.index', compact('conversas'));
    }

    public function store(Request $request)
    {
        $destinatario   = $request->input('user_id');
        $mensagem       = $request->input('mensagem');
        $ajaxRequest    = $request->input('ajax');

        $conversa = Mensagem::where('conversa_id',auth()->user()->id .'x'. $destinatario)
            ->orWhere('conversa_id',auth()->user()->id .'x'. $destinatario)
            ->limit(1)
            ->get();

        if($destinatario){
            auth()->user()->mensagensEnviadas()->create([
                'mensagem'  => $mensagem,
                'usuario_destinatario_id' => $destinatario,
                'conversa_id' => $conversa->count() ? $conversa->first()->conversa_id : auth()->user()->id .'x'. $destinatario

            ]);

            if($ajaxRequest){
                return [
                    'data' => [
                        'status' => 'success',
                        'message' => 'salvo com sucesso!',
                        'info'    => 'Recarregando em 2 segundos...',
                        'redirect' => route('search.index'),
                        'code' => 204
                    ]
                ];
            }

            return redirect()->back()->with('success', 'Mensagem enviada');
        }

        return redirect()->back()->withErrors('Erro ao salvar');
    }
}