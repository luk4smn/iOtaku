<?php

namespace App\Http\Controllers;

use App\Entities\Emprestimo;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mensagens = auth()->user()->mensagensRecebidas->where('lido',false)->count();
        $pendentes = auth()->user()->emprestimosPendentes->count();

        return view('home', compact('pendentes', 'mensagens'));
    }
}
