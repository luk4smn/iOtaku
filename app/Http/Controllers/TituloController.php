<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 25/11/18
 * Time: 21:13
 */

namespace App\Http\Controllers;


use App\Entities\Titulo;

class TituloController extends Controller
{

    public function index(){
        return [
            'titulos' => Titulo::pluck('nome','id'),
            'autores' => Titulo::pluck('autor','id'),
            'resumos' => Titulo::pluck('resumo','id'),
        ];
    }

}