//  JQUERY FUNCTIONS
$(document).ready(function () {

    $('.logout').on('click', function (e) {
        e.preventDefault();
        swal({
            title               : "Deseja fazer logout ?",
            type                : "question",
            confirmButtonColor  : "red",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                document.getElementById('logout-form').submit();
            }
        }.bind(this));
    });

    $('.delete').on('click', function (e) {
        e.preventDefault();

        let url = $(this).attr('data-action')
        let redirect = $(this).attr('data-redirect')

        swal({
            title               : "Deseja realmente deletar ?",
            type                : "question",
            confirmButtonColor  : "red",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax(
                    {
                        url: url,
                        type: 'DELETE',
                        dataType: "JSON",
                        success: function (response)
                        {
                            swal({
                                title   : response.data.message,
                                text    : response.data.info,
                                type    : response.data.status,
                                timer   : 2000,
                                showConfirmButton: false
                            },setTimeout(function() {
                                window.location.href = redirect;
                                }, 2000))
                        },
                        error: function(xhr) {
                            console.log(xhr.responseText);
                            swal({
                                title   : "Occoreu um erro ao deletar",
                                type    : "error",
                                timer   : 2000,
                                showConfirmButton: false
                            });
                        }
                    });

            }
        }.bind(this));
    });

    $('.devolver').on('click', function (e) {
        e.preventDefault();

        let url = $(this).attr('data-action')
        let redirect = $(this).attr('data-redirect')

        swal({
            title               : "Realizar devolução dos itens emprestados ?",
            type                : "question",
            confirmButtonColor  : "green",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        dataType: "JSON",
                        success: function (response)
                        {
                            swal({
                                title   : response.data.message,
                                text    : response.data.info,
                                type    : response.data.status,
                                timer   : 2000,
                                showConfirmButton: false
                            },setTimeout(function() {
                                window.location.href = redirect;
                            }, 2000))
                        },
                        error: function(xhr) {
                            console.log(xhr.responseText);
                            swal({
                                title   : "Occoreu um erro ao devolver",
                                type    : "error",
                                timer   : 2000,
                                showConfirmButton: false
                            });
                        }
                    });

            }
        }.bind(this));
    });

    $('.marcar').on('click', function (e) {
        e.preventDefault();

        let url = $(this).attr('data-action')
        let redirect = $(this).attr('data-redirect')

        swal({
            title               : "Marcar item como disponível para venda ?",
            type                : "question",
            confirmButtonColor  : "orange",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        dataType: "JSON",
                        success: function (response)
                        {
                            swal({
                                title   : response.data.message,
                                text    : response.data.info,
                                type    : response.data.status,
                                timer   : 2000,
                                showConfirmButton: false
                            },setTimeout(function() {
                                window.location.href = redirect;
                            }, 2000))
                        },
                        error: function(xhr) {
                            console.log(xhr.responseText);
                            swal({
                                title   : "Occoreu um erro ao marcar o item",
                                type    : "error",
                                timer   : 2000,
                                showConfirmButton: false
                            });
                        }
                    });

            }
        }.bind(this));
    });

    $('.desmarcar').on('click', function (e) {
        e.preventDefault();

        let url = $(this).attr('data-action')
        let redirect = $(this).attr('data-redirect')

        swal({
            title               : "Desmarcar item como disponível para venda ?",
            type                : "question",
            confirmButtonColor  : "black",
            confirmButtonText   : "Sim",
            cancelButtonText    : "Não",
            showCancelButton    : true,
        }).then(function(result) {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax(
                    {
                        url: url,
                        type: 'POST',
                        dataType: "JSON",
                        success: function (response)
                        {
                            swal({
                                title   : response.data.message,
                                text    : response.data.info,
                                type    : response.data.status,
                                timer   : 2000,
                                showConfirmButton: false
                            },setTimeout(function() {
                                window.location.href = redirect;
                            }, 2000))
                        },
                        error: function(xhr) {
                            console.log(xhr.responseText);
                            swal({
                                title   : "Occoreu um erro ao desmarcar o item",
                                type    : "error",
                                timer   : 2000,
                                showConfirmButton: false
                            });
                        }
                    });

            }
        }.bind(this));
    });

    $('.datatable-export').DataTable({
        paginate: true,
        searching : true,
        bLengthChange : true,
        aLengthMenu : [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Buscar:</span> _INPUT_',
            lengthMenu: '<span>Mostrar:</span> _MENU_',
            paginate: {'first': 'Primeiro', 'last': 'Último', 'next': 'Próximo', 'previous': 'Anterior'},
            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            sZeroRecords: "Nenhum registro encontrado",
            sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
            sEmptyTable: "Nenhum registro encontrado",
        },
    });

    $('.not-active').prop("disabled", true);

});